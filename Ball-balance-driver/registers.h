#define SMPLRT_DIV 0x19 // value 0x07'
#define SMPLRT_DIV_VAL 0x07

#define CONFIG 0x1a // value should be 0x01 (digital low pass filter)
#define CONFIG_VAL 0x01

#define GYRO_CONFIG 0x1b // value should be 0x00 
#define GYRO_CONFIG_VAL 0x00

#define ACC_CONFIG 0x1c // vlaue should be 0x00
#define ACC_CONFIG_VAL 0x00

#define PWR_MGMT 0x6b // value should be 0x00
#define PWR_MGMT_VAL 0x02

#define ACC_XH 	0x3b
#define ACC_XL	0x3c
#define ACC_YH 	0x3d
#define ACC_YL	0x3e
#define ACC_ZH 	0x3f
#define ACC_ZL	0x40

#define GYRO_XH 0x43
#define GYRO_XL 0x44
#define GYRO_YH 0x45
#define GYRo_YL 0x46