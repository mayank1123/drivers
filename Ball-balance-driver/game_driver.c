#include <linux/init.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/device.h>
#include <linux/slab.h>
#include <linux/uaccess.h>
#include <linux/gfp.h>
#include <linux/gpio.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <linux/delay.h>
#include <linux/string.h>
#include <linux/errno.h>
//#include "registers.h"
#include <linux/fs.h>
#include <linux/input.h>
#include <linux/kthread.h>
#include <linux/unistd.h>
#include <linux/time.h>


#define SMPLRT_DIV 0x19 // value 0x07'
#define SMPLRT_DIV_VAL 0x07

#define CONFIG 0x1a // value should be 0x01 (digital low pass filter)
#define CONFIG_VAL 0x07

#define GYRO_CONFIG 0x1b // value should be 0x00 
#define GYRO_CONFIG_VAL 0x00

#define ACC_CONFIG 0x1c // vlaue should be 0x00
#define ACC_CONFIG_VAL 0x00

#define PWR_MGMT 0x6b // value should be 0x00
#define PWR_MGMT_VAL 0x02

#define ACC_XH 	0x3b
#define ACC_XL	0x3c
#define ACC_YH 	0x3d
#define ACC_YL	0x3e
#define ACC_ZH 	0x3f
#define ACC_ZL	0x40

#define GYRO_XH 0x43
#define GYRO_XL 0x44
#define GYRO_YH 0x45
#define GYRo_YL 0x46

#define DEVICE_NAME "joysticker"
#define DEVICE_ADDR 0x68
#define alpha 25
#define I2CMUX 29

static dev_t i2c_dev_number;
struct class *i2c_dev_class;

static struct device *i2c_dev_device;
static struct task_struct *sensor_thread = NULL;

struct input_dev *joystick_dev;
struct fasync_struct *event_fasync = NULL;

struct i2c_dev{
	struct cdev cdev;
	struct i2c_client *client;
	struct i2c_adapter *adapter;
	int X;
	int Y;
}*i2c_devp;

long int accum_time;
long int total_time;

uint64_t GetTSCTime(void)
{
	unsigned int high, low;
	asm volatile(
		"RDTSC\n\t"
		"mov %%edx, %0\n\t"
		"mov %%eax, %1\n\t": "=r" (high), "=r" (low)::"%eax","%edx");
	return ((uint64_t)high << 32) | low;
}

//int gyro_filter();


static int sensor_function (void *sensor_devp){
	int ret ; 
	char var[2];
	char gyro[4];
	char acc[4];
	int gyro_xx;
	int gyro_yy;
	int acc_xx;
	int acc_yy;
	int acc_xold, acc_yold;
	int gyro_xold, gyro_yold;
	struct i2c_dev *devp;
	char var2,var3,var4;
	int count;

	gyro_xold = 0;
	gyro_yold = 0;
	acc_xold = 0;
	acc_yold = 0;
	var3 = 0x75;
	total_time = 0;
	devp = sensor_devp;
	count = 0;


	var[0] = CONFIG;
	var[1] = CONFIG_VAL;
	ret = i2c_master_send(devp->client, var, 2);
	
	var[0] = ACC_CONFIG;
	var[1] = ACC_CONFIG_VAL;
	//ret = i2c_master_send(devp->client, var , 2);

	var[0] = GYRO_CONFIG;
	var[1] = GYRO_CONFIG_VAL;
	//ret = i2c_master_send(devp->client, var , 2);
	
	var[0] = SMPLRT_DIV;
	var[1] = SMPLRT_DIV_VAL;
	//ret = i2c_master_send(devp->client, var , 2); 

	var[0] = PWR_MGMT;
	var[1] = PWR_MGMT_VAL;
	ret = i2c_master_send(i2c_devp->client, var, 2);

	ret = i2c_master_send(devp->client, &var3, 1);
	
	ret = i2c_master_recv(devp->client, &var4, 1);

	printk("The value read is %x \n", var4);

	
	while (!kthread_should_stop()){

		accum_time = GetTSCTime();
		var2 = GYRO_XH;
		ret = i2c_master_send(devp->client, &var2, 1);
		ret = i2c_master_recv(devp->client, gyro, 4);

		var2 = ACC_XH;
		ret = i2c_master_send(devp->client, &var2, 1); 
		ret = i2c_master_recv(devp->client, acc , 4); 

		gyro_xx = ((gyro[1] & 0xff)| gyro[0] << 8)/131;
		gyro_yy = ((gyro[3] & 0xff )| gyro[2] << 8)/131;
		gyro_xx = (alpha * gyro_xold /100 + (100-alpha) * gyro_xx)/100; 
		gyro_yy = (alpha * gyro_xold /100 + (100-alpha) * gyro_yy)/100;
		gyro_xold = gyro_xx;
		gyro_yold = gyro_yy;

		acc_xx = ((acc[1] & 0xff)| acc[0] << 8);
		acc_yy = ((acc[3] & 0xff )| acc[2] << 8);
		acc_xx = (alpha * acc_xold /100 + (100-alpha) * acc_xx)/100; 
		acc_yy = (alpha * acc_xold /100 + (100-alpha) * acc_yy)/100;
		acc_xold = acc_xx;
		acc_yold = acc_yy;
		if (count <10){
			total_time = GetTSCTime() - accum_time;
			//printk("Accumulated time is : %ld \n" ,total_time);
			count++;
		}

		//printk("acc_xx %d \n", acc_xx);
		//printk("GYRO_Y %d \n", gyro_yy);

		input_report_abs(joystick_dev, ABS_X , gyro_xx);
		input_report_abs(joystick_dev, ABS_Y , gyro_yy);
		input_report_rel(joystick_dev, REL_X , acc_xx);
		input_report_rel(joystick_dev, REL_Y , acc_yy);
		input_sync(joystick_dev);

	//	kill_fasync(&event_fasync, SIGIO, POLL_IN);
	//	mdelay(1);
	}
	return 0;
}

int i2c_fasync (int fd, struct file *filp , int on){

	int ret;
//	struct i2c_dev *devp = filp-> private_data;
	/* This is run just once to add the list of processes interested in getting a notification everytime there is an update*/
	ret = fasync_helper(fd, filp, on, &event_fasync);
	if (ret < 0 )
		return ret; 
	return 0;
} 

int i2c_open(struct inode *inode, struct file *filp){

	struct i2c_dev *devp ;
	devp = container_of (inode->i_cdev, struct i2c_dev , cdev);
	// spawn a thread that continuously listens and posts events to user space
	sensor_thread = kthread_run(sensor_function, devp ,"hoooha") ;
	filp->private_data = devp; 
	return 0;
}

int i2c_release(struct inode *inode, struct file *filp){
	// close the thread and exit 
	kthread_stop(sensor_thread); 
	return 0; 
}

ssize_t i2c_write(struct file *filp, const char *buf, size_t count, loff_t *ppos){
	//
	return 0;
}

ssize_t i2c_read(struct file *filp, char *buf, size_t count, loff_t *ppos){
	//
	return 0;
}

static struct file_operations i2c_fops = {
    .owner			= THIS_MODULE,        	  /* Owner , defined in  */
    .open			= i2c_open,        /* Open method */
    .release		= i2c_release,     /* Release method */
    .write			= i2c_write,       /* Write method */
    .read			= i2c_read,        /* Read method */
 //   .fasync 		= i2c_fasync,
};

static int __init i2c_init(void){
	int ret;
	if (alloc_chrdev_region(&i2c_dev_number,0,1,DEVICE_NAME)<0) {
		printk(KERN_DEBUG "Can't allocate device major number\n");
		return -1;
	}
	i2c_dev_class = class_create(THIS_MODULE,DEVICE_NAME);

	i2c_devp = kmalloc(sizeof(struct i2c_dev), GFP_KERNEL);

	if (!i2c_devp){
		printk(KERN_DEBUG "Bad kmalloc\n"); 
		return -ENOMEM;
	}

	cdev_init(&i2c_devp->cdev, &i2c_fops);
	i2c_devp->cdev.owner = THIS_MODULE;

	ret = cdev_add(&i2c_devp->cdev, (i2c_dev_number),1);

	if (ret<0) {
		printk("Bad cdev\n");
		return -1;
	}

	i2c_dev_device  = device_create(i2c_dev_class , NULL , MKDEV(MAJOR(i2c_dev_number),0), NULL, DEVICE_NAME);

	ret = gpio_request (I2CMUX,"I2CMUX");
	ret = gpio_direction_output(I2CMUX, 0);
	gpio_set_value_cansleep(I2CMUX, 0);

	i2c_devp->adapter = i2c_get_adapter(0);

	i2c_devp->client = (struct i2c_client *) kmalloc(sizeof(struct i2c_client),GFP_KERNEL);
	i2c_devp->client->addr = DEVICE_ADDR;
	snprintf(i2c_devp->client->name, I2C_NAME_SIZE, "gamers");
	i2c_devp->client->adapter = i2c_devp->adapter;

/*
	Wrtie the registers before starting to read data from the sensor registers
*/

	joystick_dev = input_allocate_device();
	joystick_dev->name = "example";
	joystick_dev->phys = "A/fake/path";
	joystick_dev->id.bustype = BUS_HOST;
    joystick_dev->id.vendor = 0x0001;
    joystick_dev->id.product = 0x0001;
    joystick_dev->id.version = 0x0100;


    set_bit(EV_ABS, joystick_dev->evbit);
    set_bit(ABS_X, joystick_dev->absbit);
    set_bit(ABS_Y, joystick_dev->absbit);
    set_bit(EV_REL, joystick_dev->evbit);
    set_bit(REL_X, joystick_dev->relbit);
    set_bit(REL_Y, joystick_dev->relbit);
	input_set_abs_params(joystick_dev, ABS_X, -255, 255, 0, 0); // try setting 4,8 to 0,0
	input_set_abs_params(joystick_dev, ABS_Y, -255, 255, 0, 0);

	ret = input_register_device(joystick_dev);
//	sensor_thread = kthread_run(sensor_function, i2c_devp ,"hoooha") ;
	return 0;
}

void __exit i2c_exit(void){

input_unregister_device(joystick_dev);

	if(sensor_thread){

		kthread_stop(sensor_thread);
	}

	i2c_put_adapter(i2c_devp->adapter);
	kfree(i2c_devp->client);

	unregister_chrdev_region(i2c_dev_number,1);

	device_destroy(i2c_dev_class, MKDEV(MAJOR(i2c_dev_number),0));
	cdev_del(&i2c_devp->cdev);

	kfree(i2c_devp);
	class_destroy(i2c_dev_class);

	gpio_free(I2CMUX);
}

module_init(i2c_init);
module_exit(i2c_exit);
MODULE_AUTHOR("Mayank Gupta");
MODULE_LICENSE("GPL v2");
MODULE_DESCRIPTION("CSE 436: Embedded Systems Programming, Assignment 4");
