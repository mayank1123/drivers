#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <linux/input.h>
#include <signal.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>
#include <signal.h>
#include <unistd.h>
#include <stdint.h>
#include <math.h>
#include <time.h>
#define FACTOR 16
#define KEYFILE "/dev/input/event2"
#define DEV "/dev/joysticker"
#define timer 0.01


void siginthandler(int);
int fd, device; 
struct input_event ie;



int main()
{	
	int level_no;
	float factor;
	int i =0;
	int j =0;
	int fd1 = 0;	
	int gpio2 = 0;
	int gpio3 = 0;
	int gpio4 = 0;	
	int ret = 0;
	double x,y;
	unsigned int safe_distance;
	unsigned int distance =0; 
	unsigned char mode = 0;
	unsigned char lsb = 0; 
	unsigned char readBuf[2];
	unsigned char writeBuf[2];
	struct spi_ioc_transfer xfer;
	int speed = 10000000;
	int count = 10; 	
	int flags;
	int deg_per_sec_x;
	int deg_per_sec_y;
	double angle_x, angle_y;
	double displacement_x;
	double displacement_y;
	double v_x , v_y ; 
	v_x = 0 ; 
	v_y = 0 ; 
	displacement_x = 0;
	displacement_y = 0;
	angle_x = 0;
	angle_y = 0;
	double acc_x , acc_y ; 
	double accData_x , accData_y ; 
	double angle_x_off , angle_y_off;
	double accData_x_off , accData_y_off;
	accData_x = 0; accData_y = 0;
	acc_x = 0; acc_y = 0;
	angle_y_off = 0; angle_x_off = 0;
	accData_y_off = 0; accData_x_off = 0;

	unsigned char X[8]={0x81, 0x42, 0x24, 0x18, 0x18, 0x24, 0x42, 0x81};
	unsigned char Fire[8]={0xFF, 0xFF, 0xFF, 0x81, 0x81, 0xFF, 0xFF, 0xFF};
	unsigned char starting[8]={0x00, 0x00, 0x00, 0x18, 0x18, 0x00, 0x00, 0x00};
	unsigned char level[3][8]={{0x00,0x84,0x82,0xFF,0x80,0x80,0x80,0x00},
								{0x00,0x00,0x02,0xF1,0x91,0x9E,0x80,0x00},
								{0x00,0x00,0x44,0x92,0x92,0x6E,0x00,0x00}};
	
	time_t t;
	srand((unsigned) time(&t));
	x = 1+rand()%6;
	y = 1+rand()%6;
	while ((x == 4 || x == 5) && (y == 5 || y ==4)) {
		x = 1+rand()%6 ;
		y = 1+rand()%6;
	}

	// ----------------------------------------- GPIO 43-11 - MOSI/DIN 

	gpio2 = open("/sys/class/gpio/export", O_WRONLY);
	write(gpio2,"43",2);
	close(gpio2);
	// Set GPIO Direction
	gpio2 = open("/sys/class/gpio/gpio43/direction", O_WRONLY);
	write(gpio2, "out", 3);
	close(gpio2);
	// Set Value
	gpio2 = open("/sys/class/gpio/gpio43/value", O_WRONLY);
	write(gpio2, "0", 1);	
	close(gpio2);
	// ----------------------------------------- GPIO 42-10 SPI_CS
	
	gpio2 = open("/sys/class/gpio/export", O_WRONLY);
	write(gpio2,"42",2);
	close(gpio2);
	// Set GPIO Direction
	gpio2 = open("/sys/class/gpio/gpio42/direction", O_WRONLY);
	write(gpio2, "out", 3);
	close(gpio2);
	// Set Value
	gpio2 = open("/sys/class/gpio/gpio42/value", O_WRONLY);
	write(gpio2, "0", 1);			
	close(gpio2);	
	// ----------------------------------------- GPIO 55-13 - CLK
	
	gpio3 = open("/sys/class/gpio/export", O_WRONLY);
	write(gpio3,"55",2);
	close(gpio3);
	// Set GPIO Direction
	gpio3 = open("/sys/class/gpio/gpio55/direction", O_WRONLY);
	write(gpio3, "out", 3);
	close(gpio3);
	// Set Value
	gpio3 = open("/sys/class/gpio/gpio55/value", O_WRONLY);
	write(gpio3, "0", 1);
	close(gpio3);		
	
	fd = open("/dev/spidev1.0", O_RDWR);
	mode = SPI_MODE_0; 

	if(ioctl(fd, SPI_IOC_WR_MODE, &mode) < 0)
	{
		perror("SPI Set Mode Failed");
		close(fd);
		return -1;
	}
	if(ioctl(fd, SPI_IOC_WR_LSB_FIRST, &mode) <0)
	{ 
		perror("SPI Set Mode Failed");
		close(fd);
		return -1;
	}

	// Setup Transaction 
	xfer.rx_buf = (unsigned long) NULL;
	xfer.len = 2; // Bytes to send
	xfer.cs_change = 0;
	xfer.delay_usecs = 0;
	xfer.speed_hz = 10000000;
	xfer.bits_per_word = 8;
	ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);

	// -------------------------------- decoding :BCD 
		
	writeBuf[0] = 0x09;
	writeBuf[1] = 0x00;
	xfer.tx_buf =(unsigned long) writeBuf;
	
	if(ioctl(fd, SPI_IOC_MESSAGE(1), &xfer) < 0)
		perror("SPI Message Send Failed");
	// -------------------------------- brightness 
	writeBuf[0] = 0x0a;
	writeBuf[1] = 0x03; 
	xfer.tx_buf = (unsigned long) writeBuf;	
	
	
	if(ioctl(fd, SPI_IOC_MESSAGE(1), &xfer) < 0)
		perror("SPI Message Send Failed");
	// -------------------------------- scanlimit ; 8 LEDs
	
	writeBuf[0] = 0x0b;
	writeBuf[1] = 0x07; 
	xfer.tx_buf = (unsigned long) writeBuf;
	if(ioctl(fd, SPI_IOC_MESSAGE(1), &xfer) < 0)
		perror("SPI Message Send Failed");
	// -------------------------------- power down mode : 0 , normal mode :1
	
	writeBuf[0] = 0x0c;
	writeBuf[1] = 0x01; 
	xfer.tx_buf= (unsigned long) writeBuf;
	if(ioctl(fd, SPI_IOC_MESSAGE(1), &xfer) < 0)
	perror("SPI Message Send Failed");
	// -------------------------------- test displey :1 ; EOT , display :0 
	writeBuf[0] = 0x0f;
	writeBuf[1] = 0x00; 
	xfer.tx_buf = (unsigned long) writeBuf;
	if(ioctl(fd, SPI_IOC_MESSAGE(1), &xfer) < 0)
		perror("SPI Message Send Failed");
	sleep(1);
	
	for (i = 0 ; i < 8 ; i++){
	writeBuf[0] = 0x01 + i ; 
	writeBuf[1] = 0x00 ;
	if (ioctl(fd , SPI_IOC_MESSAGE(1), &xfer) < 0)
		perror("SPI Message send failed");
	}

	signal(SIGINT, siginthandler);
	
	if((device = open(DEV, O_RDWR)) == -1){
	printf("Error opening char device \n");
	return -1;
	}

	if((fd1 = open(KEYFILE, O_RDONLY)) == -1) {
		perror("Error opening key device \n");
		exit(EXIT_FAILURE);
	}
	
	for (i=0; i<8; i++){
					writeBuf[0] = 0x01+ i;
					writeBuf[1] = Fire[i];
					xfer.tx_buf =(unsigned long) writeBuf;
					ioctl(fd,SPI_IOC_MESSAGE(1),&xfer);
				}
	
	/* For non-blocking read */
	 double timer_var ;
	 double tee =0;
	 double g = 9.83;
	flags = fcntl(fd1, F_GETFL, 0);
	fcntl(fd1, F_SETFL, flags | O_NONBLOCK);

	while(count < 20){
			read(fd1, &ie, sizeof(struct input_event));
			if (ie.type == EV_ABS && ie.code == ABS_X){
				deg_per_sec_x = ie.value ;
				tee =  deg_per_sec_x*timer ;
				angle_x += tee ;
			//	printf("Angle X is %f \n", angle_x);
			}
			else if (ie.type == EV_ABS && ie.code == ABS_Y){
				deg_per_sec_y = ie.value ;
				angle_y = deg_per_sec_y*timer + angle_y;
			//	printf("Angle Y is %f \n", angle_y);
			}

			else if (ie.type == EV_REL && ie.code == REL_X){
				acc_x = (double)(ie.value * 1.0); 
				accData_x += asin(acc_x/g)/100;
			//	printf("Angle X is %f \n", accData_x*60);
			}

			else if (ie.type == EV_REL && ie.code == REL_Y){
				acc_y = (double)(ie.value * 1.0);
				accData_y = +asin(acc_y/g)/100;
			//	printf("Angle Y is %f \n", accData_y*60);
			}
			count ++;
		}

	angle_x_off = angle_x;
	angle_y_off = angle_y;
	accData_x_off = accData_x; 
	accData_y_off = accData_y; 
	angle_x = 0; 
	angle_y = 0;
	accData_x= 0;
	accData_y = 0;
	int crap1, crap2;


/* Display the starting condition */
	for (i=0;i<5;i++){
		for (i=0; i<8; i++){
			writeBuf[0] = 0x01+ i;
			writeBuf[1] = starting[i];
			xfer.tx_buf =(unsigned long) writeBuf;
			ioctl(fd,SPI_IOC_MESSAGE(1),&xfer);
		}
		usleep(200000);
		writeBuf[0] = 0x0f;
		writeBuf[1] = 0x00; 
		xfer.tx_buf = (unsigned long) writeBuf;
		if(ioctl(fd, SPI_IOC_MESSAGE(1), &xfer) < 0) perror("SPI Message Send Failed");
		usleep(200000);
	}

	factor = 24;

	while(1){
			for (i = 0; i < 4; i++){
				read(fd1, &ie, sizeof(struct input_event));
				if (ie.type == EV_ABS && ie.code == ABS_X){
					deg_per_sec_x = ie.value ;
					tee =  deg_per_sec_x*timer ;
					if (tee > -0.1 && tee < 0.1) tee = 0.0;
					angle_x += tee - angle_x_off;
			//	printf("Angle X is %f \n", angle_x);
				}
				else if (ie.type == EV_ABS && ie.code == ABS_Y){
					deg_per_sec_y = ie.value ;
					angle_y = deg_per_sec_y*timer + angle_y - angle_y_off ;
			//	printf("Angle Y is %f \n", angle_y);
				}

				else if (ie.type == EV_REL && ie.code == REL_X){
					acc_x = (double)(ie.value * 1.0); 
					accData_x = asin(acc_x/16384) - accData_x_off; // value is per g 
				}

				else if (ie.type == EV_REL && ie.code == REL_Y){
					acc_y = (double)(ie.value * 1.0);
					accData_y = asin(acc_y/16384) - accData_y_off;
			//	printf("Angle Y is %f \n", accData_y*60);
				}
			}
			
			angle_x = angle_x*0.98 + 0.02*57.3*accData_y; // w_x means change in acc_y
			angle_y = angle_y*0.98 + 0.02*57.3*accData_x;	
			if (abs((int)angle_x)<6) {
			//	printf("loop 1 \n");
				v_x = 0;
			}
			else if (abs((int)angle_x)< 16){
			//	printf("loop 2 \n");
				v_x = 1*(int)angle_x/abs((int)angle_x);
			}

			else if (abs((int)angle_x)<26){
			//	printf("loop 3 \n");
				v_x = 2*(int)angle_x/abs((int)angle_x);
			}

			else if (abs((int)angle_x)<36){
			//	printf("loop 4 \n");
				v_x = 3*(int)angle_x/abs((int)angle_x);
			}

			if (abs((int)angle_y)<6) {
			//	printf("loop 1 \n");
				v_y = 0;
			}
			else if (abs((int)angle_y)< 16){
			//	printf("loop 2 \n");
				v_y = 1*(int)angle_y/abs((int)angle_y);
			}

			else if (abs((int)angle_y)<26){
			//	printf("loop 3 \n");
				v_y = 2*(int)angle_y/abs((int)angle_y);
			}

			else if (abs((int)angle_y)<36){
			//	printf("loop 4 \n");
				v_y = 3*(int)angle_x/abs((int)angle_y);
			}

			x = x - (v_x*timer*24/factor);
			y = y - (v_y*timer*24/factor);

			if (x<=8 && x>=1 && y>=1 && y<=8){
				/* LED is within the bounds display it on screen */
				if (((int)x==4 || (int)x ==5) && ((int)y== 4 || (int)y== 5)){
					/* LED is at the center */
					count = count + 1;
				}
				else count = 0;

				/* Light the LEDs */
				writeBuf[0] = 0x00 + (int)x;	
				writeBuf[1] = 0x00 + 1<<((int)y-1);
				xfer.tx_buf =(unsigned long) writeBuf;
				ioctl(fd,SPI_IOC_MESSAGE(1),&xfer);
				printf("X coordinate is %d  Y coordinate is %d Count is %d level is %d  factor is %d \n" , (int)x , (int)y , count, level_no, factor);
				/* Display fireworks if LED has stayed in loop for long enough */
				if (count == 300){
					count = 0;
					for (i=0; i<8 ;i++){
					writeBuf[0] = 0x01+ i;
					writeBuf[1] = 0x00;
					xfer.tx_buf =(unsigned long) writeBuf;
					ioctl(fd,SPI_IOC_MESSAGE(1),&xfer);
				}
					for (i=0; i<8; i++){
					writeBuf[0] = 0x01+ i;
					writeBuf[1] = Fire[i];
					xfer.tx_buf =(unsigned long) writeBuf;
					ioctl(fd,SPI_IOC_MESSAGE(1),&xfer);
					usleep(200000);
					printf("HURRAY ! \n" );
				}
				//level_no = 1+(level_no-1)%3;
				level_no++;
				if (level_no>3) {level_no = 0; factor = FACTOR;}
				
				else {
					factor = factor/2;
				/* Display the level */
					for (i=0;i<8;i++){
						writeBuf[0] = 0x01+ i;
						writeBuf[1] = level[level_no-1][i];
						xfer.tx_buf =(unsigned long) writeBuf;
						ioctl(fd,SPI_IOC_MESSAGE(1),&xfer);
					}
				usleep(200000);
				}
				/* Display the starting condition */
					for (i=0;i<5;i++){
						for (i=0; i<8; i++){
							writeBuf[0] = 0x01+ i;
							writeBuf[1] = starting[i];
							xfer.tx_buf =(unsigned long) writeBuf;
							ioctl(fd,SPI_IOC_MESSAGE(1),&xfer);
						}
						usleep(200000);
					}
					
					v_x = 0;
					v_y = 0;
					angle_x = 0; 
					angle_y = 0;
					x = 1+rand()%6;
					y = 1+rand()%6;
					while ((x == 4 || x == 5) && (y == 5 || y ==4)) {
						x = 1+rand()%6;
						y = 1+rand()%6;
					}
				}
				usleep(10000);
				/* Fireworks end and display is already refreshed */
			}

			else{

				/* Ball has gone off the floor so light the X */
				level_no=0;
				count = 0;
				for (j = 0 ; j<5 ; j++){
					for (i=0; i<8 ;i++){
						writeBuf[0] = 0x01+ i;
						writeBuf[1] = 0x00;
						xfer.tx_buf =(unsigned long) writeBuf;
						ioctl(fd,SPI_IOC_MESSAGE(1),&xfer);
					}

					usleep(200000);

					for (i=0; i<8; i++){
						writeBuf[0] = 0x01+ i;
						writeBuf[1] = X[i];
						xfer.tx_buf =(unsigned long) writeBuf;
						ioctl(fd,SPI_IOC_MESSAGE(1),&xfer);
						printf("Wasted !! \n");
					}
					usleep(200000);
				}
				/* Display the start condition */
				for (i=0;i<5;i++){
					for (i=0; i<8; i++){
					writeBuf[0] = 0x01+ i;
					writeBuf[1] = starting[i];
					xfer.tx_buf =(unsigned long) writeBuf;
					ioctl(fd,SPI_IOC_MESSAGE(1),&xfer);
					}
					usleep(200000);
				}

				x=1+rand()%6;
				y=1+rand()%6;
				while ((x == 4 || x == 5) && (y == 5 || y ==4)) {
					x = 1+rand()%6;
					y = 1+rand()%6;
				}
				v_x = 0;
				v_y = 0;
				angle_x=0;
				angle_y=0;
				factor = FACTOR;
			}
			for (i=0; i<8 ;i++){
					writeBuf[0] = 0x01+ i;
					writeBuf[1] = 0x00;
					xfer.tx_buf =(unsigned long) writeBuf;
					ioctl(fd,SPI_IOC_MESSAGE(1),&xfer);
			}
		}
			
	return 0;
}

void siginthandler(int signum)
{
   printf("Caught SIGINT, ending program...\n");
   close(fd);
   close(device);
   exit(1);
}

