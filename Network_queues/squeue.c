#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/types.h>
#include <linux/slab.h>
#include <asm/uaccess.h>
#include <linux/string.h>
#include <linux/device.h>

#include<linux/init.h>
#include<linux/moduleparam.h>

struct q_message
{
	long int enq_time; // Enqueue time
	long int acc_time; // Accumulated Time
	unsigned int id; // Message ID
	unsigned int src;  // Source ID
	unsigned int dest; // Destination ID
	char message[80]; // String
};

struct queue_dev
{
	struct cdev cdev;
	int qID;
	struct q_message *ringbuf[10];	
	int ring_count;
	int ring_head;
	int ring_tail;
};

uint64_t GetTSCTime(void)
{
	unsigned int high, low;
	asm volatile(
		"RDTSC\n\t"
		"mov %%edx, %0\n\t"
		"mov %%eax, %1\n\t": "=r" (high), "=r" (low)::"%eax","%edx");
	return ((uint64_t)high << 32) | low;
}




static struct queue_dev* queue_devp[4];
static dev_t bus_q_num[4];
static struct class* bus_q_class[4];
static struct device* bus_device[4];

int queue_driver_open(struct inode *inode, struct file *file)
{
	struct queue_dev *queue_devp;
	queue_devp = container_of(inode->i_cdev, struct queue_dev, cdev);

	file->private_data = queue_devp; // Transfer device data to file structure
	printk("Queue Device %d is opening.\n", queue_devp->qID);
	return 0;
}

int queue_driver_release(struct inode *inode, struct file *file)
{
	struct queue_dev *queue_devp = file->private_data;
	printk("Queue Device %d is closing.\n", queue_devp->qID);
	return 0;
}

ssize_t queue_driver_write(struct file *file, const char *buf, size_t count, loff_t *ppos)
{
	int ret = 0;
	struct queue_dev *queue_devp = file->private_data;

	// Enqueue
	printk("Device %d Received Message at %lu\n", queue_devp->qID, GetTSCTime());
	// Add to Ring Buffer
	if(queue_devp->ring_count < 10)
	{
		struct q_message* newMsg = kmalloc(sizeof(struct q_message), GFP_KERNEL);
		ret = copy_from_user((char*)newMsg, buf, sizeof(struct q_message));
		if(ret < 0)		
			return -EINVAL;
		if(newMsg->enq_time == 0)
			newMsg->enq_time = GetTSCTime();
		queue_devp->ringbuf[queue_devp->ring_head] = newMsg;
		queue_devp->ring_count++;
		queue_devp->ring_head++;
		queue_devp->ring_head %= 10;
	}
	else
		return -EINVAL;
	
	return sizeof(struct q_message); 
}

ssize_t queue_driver_read(struct file *file, char *buf, size_t count, loff_t *ppos)
{
	int ret = 0;
	// Dequeue
	struct queue_dev *queue_devp = file->private_data;

	// Remove from Ring buffer
	if(queue_devp->ring_count > 0)
	{
		// Update Accumulated time
		queue_devp->ringbuf[queue_devp->ring_tail]->acc_time = GetTSCTime() - queue_devp->ringbuf[queue_devp->ring_tail]->enq_time;
		ret = copy_to_user(buf, (char*)queue_devp->ringbuf[queue_devp->ring_tail], sizeof(struct q_message));
		if(ret < 0)
		   return -EINVAL;
		kfree(queue_devp->ringbuf[queue_devp->ring_tail]);
		queue_devp->ring_count--;
		queue_devp->ring_tail++;
		queue_devp->ring_tail %= 10;
	}	
	else
		return -EINVAL;
	
	return sizeof(struct q_message); 
}

// Create File operations structure
static struct file_operations queue_fops = {
	.owner = THIS_MODULE,
	.open  = queue_driver_open,
	.release = queue_driver_release,
	.write = queue_driver_write,
	.read = queue_driver_read
};

// Driver Initialization
int __init queue_driver_init(void)
{
	int i;
	int ret;
	char deviceName[15];
	for(i=0; i<4; i++)
	{
		// Make device name
		if(i==0)
			sprintf(deviceName, "bus_in_q");
		else
			sprintf(deviceName, "bus_out_q%d", i);

		// Allocate device number
		ret = alloc_chrdev_region(&bus_q_num[i], 0, 1, deviceName);
		if(ret < 0)
		{
			printk("Can't register bus_in device: %d\n", ret);
			return -1;
		}

		// Populate sysfs entries
		bus_q_class[i] = class_create(THIS_MODULE, deviceName);
		
		// Allocate memory for per-device structure
		queue_devp[i] = kmalloc(sizeof(struct queue_dev), GFP_KERNEL);
		if(!queue_devp[i])
		{
			printk("Bad Kmalloc at %d\n", i);
			return -ENOMEM;
		}

		// Init File Ops in Cdev
		cdev_init(&queue_devp[i]->cdev, &queue_fops);
		queue_devp[i]->cdev.owner = THIS_MODULE;

		// Connect Major & Minor Numbers
		ret = cdev_add(&queue_devp[i]->cdev, bus_q_num[i], 1);
		if(ret) {
			printk("Bad Cdev Add Queue:%d\n", i);
			return ret;
		}

		bus_device[i] = device_create(bus_q_class[i], NULL, MKDEV(MAJOR(bus_q_num[i]), 0), NULL, deviceName);

		// Do device memory initialization here.
		queue_devp[i]->qID = i;
		queue_devp[i]->ring_count = 0;
		queue_devp[i]->ring_head = 0;
		queue_devp[i]->ring_tail = 0;
			
	}

	printk("Bus Driver Initialized.\n");
	return 0;
}


void __exit queue_driver_exit(void)
{
	int i;
	for(i=0; i<4; i++)
	{
		unregister_chrdev_region(bus_q_num[i], 1);
		device_destroy(bus_q_class[i], MKDEV(MAJOR(bus_q_num[i]),0));
		cdev_del(&queue_devp[i]->cdev);
		// Flush Buffers
		while(queue_devp[i]->ring_count > 0)
		{
			kfree(queue_devp[i]->ringbuf[queue_devp[i]->ring_tail]);
			queue_devp[i]->ring_count--;
			queue_devp[i]->ring_tail++;
			queue_devp[i]->ring_tail %= 10;
		}

		kfree(queue_devp[i]);
		class_destroy(bus_q_class[i]);		
	}
	printk("Bus Driver Destroyed.\n");
}

module_init(queue_driver_init);
module_exit(queue_driver_exit);
MODULE_LICENSE("GPL");

