#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

struct q_message
{
	long int enq_time; // Enqueue time
	long int acc_time; // Accumulated Time
	unsigned int id; // Message ID
	unsigned int src;  // Source ID
	unsigned int dest; // Destination ID
	char message[80]; // String
};

void init_qmsg(struct q_message* msg)
{
	msg->enq_time = 0;
	msg->acc_time = 0;
	msg->id = 0;
	msg->src = 0;
	msg->dest = 0;
	sprintf(msg->message, "");
}

int randRange(int low, int high)
{
	return rand()%(1+high-low) + low;
}

int main(int argc, char **argv)
{
	unsigned long tStart = clock();
	int infd;
	int outfd[3];
	int i;
	char deviceName[20];
	struct q_message msg;
	// Open Input Device
	infd = open("/dev/bus_in_q", O_RDONLY);
	if(infd < 0)
	{
		printf("Daemon Thread could not open input device.\n");
		return;
	}
	fprintf(stderr, "Daemon Opened Input queue\n");

	// Open Output Devices
	for(i=0; i<3; i++)
	{
		sprintf(deviceName, "/dev/bus_out_q%d", i+1);
		fprintf(stderr, "Daemon Attempting open %s\n", deviceName);
		
		outfd[i] = open(deviceName, O_WRONLY);
		if(outfd[i] < 0)
		{
			fprintf(stderr, "Daemon Thread could not open device %s\n", deviceName);
			return;
		}
		fprintf(stderr, "Daemon Opened Output %s\n", deviceName);
	}

	sleep(1);

	tStart = clock();
	// For 10 Seconds
	while((clock() - tStart)/CLOCKS_PER_SEC < 10)
	{
		// While Read from Input queue successful
		while(read(infd, (char*)&msg, sizeof(struct q_message)) == 0)
		{
			fprintf(stderr, "Daemon Received Msg from: %d to %d\n", msg.src, msg.dest);
			// Write to destination output queue
			while(write(outfd[msg.dest-1], (char*)&msg, sizeof(struct q_message)) < 0)
			{
				usleep(randRange(1000, 10000));
			}
			fprintf(stderr, "Daemon Sent Message to %d\n", msg.dest);
		}
		// Nap random time between 1ms and 10ms
		usleep(randRange(1000, 10000));
	}		
	// Close Devices
	close(infd);
	for(i=0; i<3; i++)
		close(outfd[i]);


	return 0;
}
