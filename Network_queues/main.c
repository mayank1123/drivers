#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <unistd.h>

struct q_message
{
	long int enq_time; // Enqueue time
	long int acc_time; // Accumulated Time
	unsigned int id; // Message ID
	unsigned int src;  // Source ID
	unsigned int dest; // Destination ID
	char message[80]; // String
};

int msg_count = 0;
pthread_mutex_t msg_mutex = PTHREAD_MUTEX_INITIALIZER;

void init_qmsg(struct q_message* msg)
{
	msg->enq_time = 0;
	msg->acc_time = 0;
	msg->id = 0;
	msg->src = 0;
	msg->dest = 0;
	sprintf(msg->message, "");
}

int randRange(int low, int high)
{
	return rand()%(1+high-low) + low;
}


void *send_thread(void *params)
{
	unsigned long tStart = clock();
	int fd = 0;
	struct q_message msg;
	int sender = *(int*)params;
	// Open Device
	fd = open("/dev/bus_in_q", O_WRONLY);
	if(fd < 0)
	{
		printf("Send Thread could not open device.\n");
		return;
	}

	sleep(1);

	// For 10 Seconds
	tStart = clock();
	while((clock() - tStart)/CLOCKS_PER_SEC < 10)
	{
		// Nap random time between 1ms and 10ms
		usleep(randRange(1000, 10000));
		// Create Message (mutex lock)
		pthread_mutex_lock(&msg_mutex);

		init_qmsg(&msg);
		msg.src = sender;
		msg.dest = randRange(1,3);
		msg.id = msg_count;
		sprintf(msg.message, "Hello, this is message #%d.", msg_count);
		msg_count++;
		
		// Send Message (mutex unlock)
		while(write(fd, (char*)&msg, sizeof(struct q_message)) < 0)
		{
			usleep(randRange(1000, 10000));
		}
		fprintf(stderr, "Sender %d - Msg #%d\n", sender, msg_count);
		pthread_mutex_unlock(&msg_mutex);	
	}
	// Close Device
	close(fd);
}

void *daemon_thread(void *params)
{
	unsigned long tStart = clock();
	int infd;
	int outfd[3];
	int i;
	char deviceName[20];
	struct q_message msg;
	// Open Input Device
	infd = open("/dev/bus_in_q", O_RDONLY);
	if(infd < 0)
	{
		printf("Daemon Thread could not open input device.\n");
		return;
	}
	fprintf(stderr, "Daemon Opened Input queue\n");

	// Open Output Devices
	for(i=0; i<3; i++)
	{
		sprintf(deviceName, "/dev/bus_out_q%d", i+1);
		fprintf(stderr, "Daemon Attempting open %s\n", deviceName);
		
		outfd[i] = open(deviceName, O_WRONLY);
		if(outfd[i] < 0)
		{
			fprintf(stderr, "Daemon Thread could not open device %s\n", deviceName);
			return;
		}
		fprintf(stderr, "Daemon Opened Output %s\n", deviceName);
	}

	sleep(1);

	tStart = clock();
	// For 11 Seconds
	while((clock() - tStart)/CLOCKS_PER_SEC < 11)
	{
		// While Read from Input queue successful
		while(read(infd, (char*)&msg, sizeof(struct q_message)) > 0)
		{
			fprintf(stderr, "Daemon Received Msg from: %d to %d\n", msg.src, msg.dest);
			// Write to destination output queue
			while(write(outfd[msg.dest-1], (char*)&msg, sizeof(struct q_message)) < 0)
			{
				usleep(randRange(1000, 10000));
			}
			fprintf(stderr, "Daemon Sent Message to %d\n", msg.dest);
		}
		// Nap random time between 1ms and 10ms
		usleep(randRange(1000, 10000));
	}		
	// Close Devices
	close(infd);
	for(i=0; i<3; i++)
		close(outfd[i]);

	return;	
}

void *receiver_thread(void *params)
{
	unsigned long tStart = clock();
	int fd = 0;
	char deviceName[20];
	int devNum = *(int*)params;
	struct q_message msg;
	// Open Device
	sprintf(deviceName, "/dev/bus_out_q%d", devNum);
	fd = open(deviceName, O_RDONLY);
	if(fd < 0)
	{
		printf("Receiver Thread could not open device.\n");
		return;
	}

	sleep(1);
	// For 11 Seconds
	tStart = clock();
	while((clock() - tStart)/CLOCKS_PER_SEC < 11)
	{
		// While Read from Input queue successful
		while(read(fd, (char*)&msg, sizeof(struct q_message)) > 0)
		{ 
			// Print message data (Src, Destination, accumulated time)
			printf("Recieved Msg.  Src: %d, Dest: %d, Time: %ul\n", msg.src, msg.dest, msg.acc_time);
		}
			
		// Nap random time between 1ms and 10 ms
		usleep(randRange(1000, 10000));
	}
	close(fd);
	return;
}

int main(int argc, char **argv)
{
	srand(time(0));
	pthread_t thread_handle[7];
	int thread_id[7] = {1,2,3,0,1,2,3};
	int i;
	int ret;

	// Spin off threads
	for(i=0; i<3; i++)
	{
		ret = pthread_create(&thread_handle[i], NULL, send_thread, (void*)&thread_id[i]);
		if(ret)
		{
			fprintf(stderr, "Error - pthread_create(%d): %d\n", i, ret);
			return 0;
		}
		fprintf(stderr, "Sender Thread %d created.\n", i);
	}	

	ret = pthread_create(&thread_handle[3], NULL, daemon_thread, (void*)&thread_id[3]);
	if(ret)
	{
		fprintf(stderr, "Error - pthread_create(3): %d\n", ret);
		return 0;
	}
	fprintf(stderr, "Daemon Thread Created.\n");

	for(i=4; i<7; i++)
	{
		ret = pthread_create(&thread_handle[i], NULL, receiver_thread, (void*)&thread_id[i]);
		if(ret)
		{
			fprintf(stderr, "Error - pthread_create(%d): %d\n", i, ret);
			return 0;
		}
	}	
	// Wait for threads to finish (joins)

	for(i=0; i<7; i++)
		pthread_join(thread_handle[i], NULL); 

	return 0;
}
