#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/types.h>
#include <linux/slab.h>
#include <asm/uaccess.h>
#include <linux/string.h>
#include <linux/device.h>
#include <linux/fcntl.h>
#include <linux/kthread.h>
#include <linux/init.h>
#include <linux/moduleparam.h>
#include <linux/delay.h>
#include <linux/gpio.h>
#include <asm/gpio.h>
#include <linux/unistd.h>
#include <linux/interrupt.h>
#include <linux/delay.h>
#define button_pin 15	// PIN 3
#define trigger_pin 14 // PIN 2
#define DEVICE_NAME  "my_dev"  // device name to be created and registered

static volatile long int enq_time = 0;
static volatile long int deq_time =0;
static unsigned int distance;
static volatile int state =0;

static struct task_struct * LEDThread = NULL;

static dev_t my_dev_number;      /* Allotted device number */
struct class *my_dev_class;          /* Tie with the device model */
static struct device *my_dev_device;

struct my_dev {
	struct cdev cdev;
	char name[20];
} *my_devp;

static void print_distance(void){
	distance = (unsigned int)((deq_time - enq_time)/23200);
//	printk("Current distance is %u \n",distance );
}

/*
void timer_task(unsigned long data)
{
	struct timer_list *timer = (struct timer_list*)data;
	print_distance();
		if (count <100){
	printk("Hello, the time is %lu\n", jiffies);
	gpio_set_value(trigger_pin,1);
	udelay(10);
	gpio_set_value(trigger_pin,0);
	timer->expires = jiffies+60;
	add_timer(timer); // Do it again!
	count++;
	}
}
*/

static int LED_BH(void *data)
{
	while(!kthread_should_stop())
	{
		// send trigger
		gpio_set_value(trigger_pin,1);
		udelay(10);
		gpio_set_value(trigger_pin,0);
		msleep(70);
		print_distance();
	}
	return 0;
}

uint64_t GetTSCTime(void)
{
	unsigned int high, low;
	asm volatile(
		"RDTSC\n\t"
		"mov %%edx, %0\n\t"
		"mov %%eax, %1\n\t": "=r" (high), "=r" (low)::"%eax","%edx");
	return ((uint64_t)high << 32) | low;
}

static irq_handler_t button_handler(int irq, void *dev_id, struct pt_regs *regs)
{
	if (state ==0){
	enq_time = GetTSCTime();
	irq_set_irq_type(irq,IRQF_TRIGGER_FALLING);
	state =1;
	}
	else{
	deq_time = GetTSCTime();
	irq_set_irq_type(irq,IRQF_TRIGGER_RISING);
	state =0;
	}
	return (irq_handler_t)IRQ_HANDLED;	
}

/*
* Open driver
*/
int my_driver_open(struct inode *inode, struct file *file)
{
	struct my_dev *my_devp;

	/* Get the per-device structure that contains this cdev */
	my_devp = container_of(inode->i_cdev, struct my_dev, cdev);

	/* Easy access to my_devp from rest of the entry points */
	file->private_data = my_devp;
	
	LEDThread = kthread_run(LED_BH, NULL,"int_test");
	return 0;
}

/*
 * Release driver
 */
int my_driver_release(struct inode *inode, struct file *file)
{
//	struct my_dev *my_devp = file->private_data;
	kthread_stop(LEDThread);	
	return 0;
	}

/*
 * Write to driver
 */
ssize_t my_driver_write(struct file *file, const char *buf,
           size_t count, loff_t *ppos)
{
//	struct my_dev *my_devp = file->private_data;
	
	return 0;
}

/*
 * Read from driver
 */
ssize_t my_driver_read(struct file *file, char *buf, size_t count, loff_t *ppos)
{
//	int distance = 0;
	int ret = 0;
//	struct my_dev *my_devp = file->private_data;
//	distance = (deq_time-enq_time)/23200;
//	printk ("Distance sent : %d \n" , distance);
	ret = copy_to_user(buf,&distance,sizeof(distance));
	return 0; 
}

/* File operations structure. Defined in linux/fs.h */
static struct file_operations my_fops = {
    .owner		= THIS_MODULE,           /* Owner */
    .open		= my_driver_open,        /* Open method */
    .release	= my_driver_release,     /* Release method */
    .write		= my_driver_write,       /* Write method */
    .read		= my_driver_read,        /* Read method */
};

static int __init init_inttest(void)
{
	int ret = 0;
	unsigned int button_irq = 0;

	/* Request dynamic allocation of a device major number */
	if (alloc_chrdev_region(&my_dev_number, 0, 1, DEVICE_NAME) < 0) {
			printk(KERN_DEBUG "Can't register device\n"); return -1;
	}

	/* Populate sysfs entries */
	my_dev_class = class_create(THIS_MODULE, DEVICE_NAME);

	/* Allocate memory for the per-device structure */
	my_devp = kmalloc(sizeof(struct my_dev), GFP_KERNEL);
		
	if (!my_devp) {
		printk("Bad Kmalloc\n"); return -ENOMEM;
	}

	/* Request I/O region */

	/* Connect the file operations with the cdev */
	cdev_init(&my_devp->cdev, &my_fops);
	my_devp->cdev.owner = THIS_MODULE;

	/* Connect the major/minor number to the cdev */
	ret = cdev_add(&my_devp->cdev, (my_dev_number), 1);

	if (ret) {
		printk("Bad cdev\n");
		return ret;
	}

	/* Send uevents to udev, so it'll create /dev nodes */
	my_dev_device = device_create(my_dev_class, NULL, MKDEV(MAJOR(my_dev_number), 0), NULL, DEVICE_NAME);	
	
	//PIN 14
	ret = gpio_request(trigger_pin, "Trigger");
	if(ret < 0)
	{
		printk("Error Requesting LED Pin.\n");

		return -1;
	}

	ret = gpio_direction_output(trigger_pin,0);
	gpio_set_value(trigger_pin,0);
	
	//PIN 15 
	ret = gpio_request(button_pin, "Interrupt_response");
	if(ret < 0)
	{
		printk("Error Requesting GPIO2_Mux.\n");
		return -1;
	}
	ret = gpio_direction_input(button_pin);
	
	//PIN 31 & 0 for PIN 2 - PIN 14  
	ret = gpio_request(0, "gpio_2_puMux");
	ret = gpio_request(31,"gpio_1_MUX");
	ret = gpio_direction_output(0,0);
	ret = gpio_direction_output(31,0);
	gpio_set_value_cansleep(0,0);
	gpio_set_value_cansleep(31,0);

	
	//PIN 30 & 1 for PIN 3 - PIN 15
	ret = gpio_request(30,"gpio_3_MUX");
	ret = gpio_request(1,"gpio_4_MUX");	
	ret = gpio_direction_output(1,0);
	ret = gpio_direction_output(30,0);
	gpio_set_value_cansleep(30,0);
	gpio_set_value_cansleep(1,0);

	button_irq = gpio_to_irq(button_pin);
	printk("Button IRQ: %d\n", button_irq);
	
	ret = request_irq(button_irq, (irq_handler_t)button_handler, IRQF_TRIGGER_RISING, "Button_Dev", (void *)(button_irq));
	if(ret < 0)
	{
		printk("Error requesting IRQ: %d\n", ret);
	}
	
/*	ret = mod_timer( &my_timer, jiffies + msecs_to_jiffies(60) );
	my_timer.function = timer_task;
	my_timer.expires = jiffies + 60;
	my_timer.data = (unsigned long)&my_timer; // :-)
	init_timer(&my_timer);
	printk("Adding timer at %lu\n", jiffies);
	add_timer(&my_timer); */
	return 0;
}

static void __exit exit_inttest(void)
{
//	int ret;	
	unsigned int button_irq = 0;
	
	/* Release the major number */
	unregister_chrdev_region((my_dev_number), 1);

	/* Destroy device */
	device_destroy (my_dev_class, MKDEV(MAJOR(my_dev_number), 0));
	cdev_del(&my_devp->cdev);
	kfree(my_devp);
	gpio_free(30);
	gpio_free(31);
	gpio_free(0);
	gpio_free(1);
	gpio_free(trigger_pin);
	gpio_free(button_pin);

	/* Destroy driver_class */
	class_destroy(my_dev_class);

	button_irq = gpio_to_irq(button_pin);
	free_irq(button_irq, (void *)(button_irq));
	//	ret = del_timer(&my_timer);
}

module_init(init_inttest);
module_exit(exit_inttest);
MODULE_LICENSE("GPL v2");