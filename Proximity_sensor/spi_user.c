#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>
#include <signal.h>
int dev1;
int dev2;

void sighandler(int);

void sighandler(int signum){
	int i;
	unsigned char writeBuf[2];
	struct spi_ioc_transfer xfer;
	int speed = 10000000;
	close (dev2);
	xfer.rx_buf = (unsigned long) NULL;
	xfer.len = 2; // Bytes to send
	xfer.cs_change = 0;
	xfer.delay_usecs = 0;
	xfer.speed_hz = 10000000;
	xfer.bits_per_word = 8;
	ioctl(dev1, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
	for (i = 0 ; i < 7 ; i++) {
		writeBuf[0] = 0x01+i ;
		writeBuf[1] = 0x00;
		if(ioctl(dev1, SPI_IOC_MESSAGE(1), &xfer) < 0)
			perror("SPI Message Send Failed");
	}
	close(dev1);
	close(dev2);
	exit(1);
}

int main(int argc, char* argv[])
{
	int i =0;
	int fd = 0;
	int fd1 = 0;	
	int gpio2 = 0;
	int gpio3 = 0;
	int gpio4 = 0;	
	int ret = 0;
	unsigned int safe_distance;
	unsigned int distance =0 ; 
	unsigned char mode = 0;
	unsigned char lsb = 0; 
	unsigned char readBuf[2];
	unsigned char writeBuf[2];
	struct spi_ioc_transfer xfer;
	int speed = 10000000;
	int count = 10 ; 
	signal(SIGINT, sighandler);		
	
	// ----------------------------------------- GPIO 43-11 - MOSI/DIN 

	gpio2 = open("/sys/class/gpio/export", O_WRONLY);
	write(gpio2,"43",2);
	close(gpio2);
	// Set GPIO Direction
	gpio2 = open("/sys/class/gpio/gpio43/direction", O_WRONLY);
	write(gpio2, "out", 3);
	close(gpio2);
	// Set Value
	gpio2 = open("/sys/class/gpio/gpio43/value", O_WRONLY);
	write(gpio2, "0", 1);	
	close(gpio2);
	// ----------------------------------------- GPIO 42-10 SPI_CS
	
	gpio2 = open("/sys/class/gpio/export", O_WRONLY);
	write(gpio2,"42",2);
	close(gpio2);
	// Set GPIO Direction
	gpio2 = open("/sys/class/gpio/gpio42/direction", O_WRONLY);
	write(gpio2, "out", 3);
	close(gpio2);
	// Set Value
	gpio2 = open("/sys/class/gpio/gpio42/value", O_WRONLY);
	write(gpio2, "0", 1);			
	close(gpio2);	
	// ----------------------------------------- GPIO 55-13 - CLK
	
	gpio3 = open("/sys/class/gpio/export", O_WRONLY);
	write(gpio3,"55",2);
	close(gpio3);
	// Set GPIO Direction
	gpio3 = open("/sys/class/gpio/gpio55/direction", O_WRONLY);
	write(gpio3, "out", 3);
	close(gpio3);
	// Set Value
	gpio3 = open("/sys/class/gpio/gpio55/value", O_WRONLY);
	write(gpio3, "0", 1);
	close(gpio3);		
	
	fd = open("/dev/spidev1.0", O_RDWR);
	fd1 = open("/dev/my_dev", O_RDWR);
	dev1 = fd;
	dev2 = fd1;
	mode = SPI_MODE_0;  // CPOL =0 , CPHA = 0		

	if(ioctl(fd, SPI_IOC_WR_MODE, &mode) < 0)
	{
		perror("SPI Set Mode Failed");
		close(fd);
		return -1;
	}
	if(ioctl(fd, SPI_IOC_WR_LSB_FIRST, &mode) <0)
	{ 
		perror("SPI Set Mode Failed");
		close(fd);
		return -1;
	}

	// Setup Transaction 
	xfer.rx_buf = (unsigned long) NULL;
	xfer.len = 2; // Bytes to send
	xfer.cs_change = 0;
	xfer.delay_usecs = 0;
	xfer.speed_hz = 10000000;
	xfer.bits_per_word = 8;
	ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);

	// -------------------------------- decoding :BCD 
		
	writeBuf[0] = 0x09;
	writeBuf[1] = 0x00;
	xfer.tx_buf =(unsigned long) writeBuf;
	
	if(ioctl(fd, SPI_IOC_MESSAGE(1), &xfer) < 0)
		perror("SPI Message Send Failed");
	// -------------------------------- brightness 
	writeBuf[0] = 0x0a;
	writeBuf[1] = 0x01; 
	xfer.tx_buf = (unsigned long) writeBuf;	
	
	
	if(ioctl(fd, SPI_IOC_MESSAGE(1), &xfer) < 0)
		perror("SPI Message Send Failed");
	// -------------------------------- scanlimit ; 8 LEDs
	
	writeBuf[0] = 0x0b;
	writeBuf[1] = 0x07; 
	xfer.tx_buf = (unsigned long) writeBuf;
	if(ioctl(fd, SPI_IOC_MESSAGE(1), &xfer) < 0)
		perror("SPI Message Send Failed");
	// -------------------------------- power down mode : 0 , normal mode :1
	
	writeBuf[0] = 0x0c;
	writeBuf[1] = 0x01; 
	xfer.tx_buf= (unsigned long) writeBuf;
	if(ioctl(fd, SPI_IOC_MESSAGE(1), &xfer) < 0)
	perror("SPI Message Send Failed");
	// -------------------------------- test displey :1 ; EOT , display :0 
	writeBuf[0] = 0x0f;
	writeBuf[1] = 0x00; 
	xfer.tx_buf = (unsigned long) writeBuf;
	if(ioctl(fd, SPI_IOC_MESSAGE(1), &xfer) < 0)
		perror("SPI Message Send Failed");
	sleep(1);
	
	for (i = 0 ; i < 8 ; i++){
	writeBuf[0] = 0x01 + i ; 
	writeBuf[1] = 0x00 ;
	if (ioctl(fd , SPI_IOC_MESSAGE(1), &xfer) < 0)
		perror("SPI Message send failed");
	}
		
	safe_distance = 5;
	if (argc > 1)
	{ safe_distance = (unsigned int)atoi(argv[1]); } 
	printf ("Safe distance is : %d\n", safe_distance);

	unsigned char display [5][8] = {
	{0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff},  // farthest
	{0x00,0xff,0xff,0xff,0xff,0xff,0xff,0x00},  
	{0x00,0x00,0xff,0xff,0xff,0xff,0x00,0x00},
	{0x00,0x00,0x00,0xff,0xff,0x00,0x00,0x00},
	{0x81,0x42,0x24,0x18,0x18,0x24,0x42,0x81}  // nearest
	};
	
	while(1) {
		//	printf ("inside:  %d\n",count);	
		ret  = read(fd1, &distance , sizeof(distance));	
	//	printf("Distance is : \n", distance);
		if (distance > 4*safe_distance) {
				for (i=0; i<8;i++){
					writeBuf[0] = 0x01+i;
					writeBuf[1] = display[0][i];
					if(ioctl(fd,SPI_IOC_MESSAGE(1),&xfer) < 0) 
					perror("SPI Message Send Failed");
				}		
				}
		if (distance <= 4*safe_distance && distance > 3*safe_distance){
				for (i=0; i<8;i++){
					writeBuf[0] = 0x01+i;
					writeBuf[1] = display[1][i];
					if(ioctl(fd,SPI_IOC_MESSAGE(1),&xfer) < 0) 
					perror("SPI Message Send Failed");
		}
		}
		if (distance <= 3*safe_distance && distance > 2*safe_distance){
				for (i=0; i<8;i++){
					writeBuf[0] = 0x01+i;
					writeBuf[1] = display[2][i];
					if(ioctl(fd,SPI_IOC_MESSAGE(1),&xfer) < 0) 
					perror("SPI Message Send Failed");
		}
		}
		if (distance <= 2*safe_distance && distance > safe_distance){
				for (i=0; i<8;i++){
					writeBuf[0] = 0x01+i;
					writeBuf[1] = display[3][i];
					if(ioctl(fd,SPI_IOC_MESSAGE(1),&xfer) < 0) 
					perror("SPI Message Send Failed");
		}
		}
		if (distance <= safe_distance){
				for (i=0; i<8;i++){
					writeBuf[0] = 0x01+i;
					writeBuf[1] = display[4][i];
					if(ioctl(fd,SPI_IOC_MESSAGE(1),&xfer) < 0) 
					perror("SPI Message Send Failed");
		}
		usleep(100000);
				for (i=0; i<8;i++){
				writeBuf[0] = 0x01+i;
				writeBuf[1] = 0x00;
				if(ioctl(fd,SPI_IOC_MESSAGE(1),&xfer)<0)
					perror("SPI Message Send Failed");
					}
		usleep(100000);
		}

		/*	if (distance < 5) {
				for (i=0;i<8;i++)
				{
	 
				}
						}	*/
	}
//	close(fd);
//	close(fd1); 
}
