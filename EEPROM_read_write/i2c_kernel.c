#include <linux/module.h>  // Module Defines and Macros (THIS_MODULE)
#include <linux/kernel.h>  // 
#include <linux/fs.h>	   // Inode and File types
#include <linux/cdev.h>    // Character Device Types and functions.
#include <linux/types.h>
#include <linux/slab.h>	   // Kmalloc/Kfree
#include <asm/uaccess.h>   // Copy to/from user space
#include <linux/string.h>
#include <linux/device.h>  // Device Creation / Destruction functions
#include <linux/i2c.h>     // i2c Kernel Interfaces
#include <linux/i2c-dev.h>
#include <linux/gpio.h>
#include <asm/gpio.h>
#include <asm/uaccess.h>
#include <linux/delay.h>
#include<linux/init.h>
#include<linux/moduleparam.h> // Passing parameters to modules through insmod
#define DEVICE_NAME "i2c_flash"  // device name to be created and registered
#define DEVICE_ADDR 0x54
#define SIZE 2
#define I2CMUX 29 // Clear for I2C function
#include "flash_dev.h"
#include <linux/workqueue.h>
#include <linux/jiffies.h>
// #include <linux/spinlock.h>

/* per device structure */
struct tmp_dev {
	struct cdev cdev;               /* The cdev structure */
	// Local variables
	struct i2c_client *client;
	struct i2c_adapter *adapter;
	int addr;
	int read_queued;  // if already a read queue running waiting for something to read 
	int busy;
//	int read_busy; // two queues one for read and one for write 
	int diff;
	int msg_ready;
	unsigned char *read_msg;
	struct workqueue_struct *wq;
	struct workqueue_struct *rq;
} *tmp_devp;

static dev_t tmp_dev_number;      /* Allotted device number */
struct class *tmp_dev_class;          /* Tie with the device model */
static struct device *tmp_dev_device;

/* Work Struct */

typedef struct {
	struct work_struct my_work;
	unsigned char *buf;
	int cnt;
	struct tmp_dev *devp;
}tmp_dev_work;

/* Work function / Write function */

void tmp_write_function(struct work_struct *work)
{
	tmp_dev_work *my_work = (tmp_dev_work *)work;
	struct tmp_dev *devp = my_work->devp ;
	
	unsigned char msg[64];
	unsigned char addr_low;
	unsigned char addr_high;
	unsigned char buffer[66];
	int ret;
	unsigned char *buf;
	int cnt  = my_work->cnt;
	printk("Inside write\n");
	buf = my_work->buf;
	addr_low = 0x00;
	addr_high = 0x00;
	ret = 0;

	/* Run the loop count no. of times */
	while (cnt >0){
		strncpy(msg,buf,63);
		msg[63] = '\0';
		printk("Count : %d , Busy :%d\n", cnt,devp->busy);
		/* Read the current write address */
		printk("Current Address %d \n", devp->addr);

		addr_high = (unsigned char)((devp->addr & 0x0000ff00UL) >>  8);
		addr_low = (unsigned char)(devp->addr & 0x000000ffUL);
		printk("Address high %d \n", addr_high);
		printk("Address low %d \n ", addr_low);

		/* Copy the message from user */
		printk(KERN_ALERT"Message received :%s:",msg);

		/* Join the address and message and write to i2c */
		sprintf(buffer,"%c%c%s",addr_high,addr_low,msg);	
		
		ret = i2c_master_send(devp->client, buffer, sizeof(buffer));  
		
		if(ret < 0)
		{
			printk(KERN_ALERT"Error: could not write \n");
		}
		
		cnt=cnt-1;
		devp->addr = (devp->addr+64)%32768;
		buf = buf +64; 
		msleep(10);
	}
	/* Reset the busy status*/
	devp->busy = 0;	
	gpio_set_value_cansleep(27, 0);

	printk("Buffer removing from : %p\n", my_work->buf);
	kfree(my_work->buf);
	kfree(my_work);
	printk("write work complete \n");
	printk("Busy Status : %d\n",devp->busy);
	return;
}

void tmp_read_function(struct work_struct *work)
{
	tmp_dev_work *my_work = (tmp_dev_work *)work;
	struct tmp_dev *devp = my_work->devp ;

	/* Write to the i2c device */
	
	/* Acquire variables needed to write */
	unsigned char addr_low;
	unsigned char addr_high;
	unsigned char r_addr[2];
	int ret;
	unsigned char *buf;
	int counter;
	int cnt  = my_work->cnt;
	addr_low = 0x00;
	addr_high = 0x00;
	ret = 0;
	devp->read_queued =1;
	counter =0;
	devp->busy= 1;	
	gpio_set_value_cansleep(27, 1);

	buf = (unsigned char * )kmalloc(sizeof(unsigned char)*64*cnt, GFP_KERNEL);

		printk("Reading from i2c\n");

		/* Read the current write address */
		addr_low =(unsigned char) (0x00 + devp->addr % 256) ;
		addr_high = (unsigned char) (0x00 + devp->addr/256) ;
		sprintf(r_addr,"%c%c",addr_high,addr_low) ;	
		printk("write address high %x\n",addr_high);
		printk("write_address low %x\n",addr_low);
		
		// Write read address and check for EEPROM busy before doing that
		while (i2c_master_send(devp->client, r_addr, sizeof(r_addr))<2) ;
	
		ret = i2c_master_recv(devp->client,buf,sizeof(char)*64*cnt);	
		printk("Message read from EEPROM %s\n",buf); 

		if(ret < 64*cnt)
		{
			printk("Error: could not read \n");
			return ;
		}
		devp->addr = (devp->addr+64*cnt)%32768;

		devp->diff = devp->diff - cnt ; 
			
	/* Reset the busy status*/
	devp->busy = 0;
	gpio_set_value_cansleep(27, 0);
	devp->read_queued = 0;
	devp->msg_ready =1;
	devp->read_msg = buf;
	
	/* Free allocated memory*/
	kfree(my_work);
	printk("Read done\n ");
	return;
}

/*
 * Open driver
 */


int tmp_driver_open(struct inode *inode, struct file *file)
{
	struct tmp_dev *tmp_devp;

	/* Get the per-device structure that contains this cdev */
	tmp_devp = container_of(inode->i_cdev, struct tmp_dev, cdev);

	/* Easy access to tmp_devp from rest of the entry points */
	tmp_devp->busy = 0;
	tmp_devp->addr = 0x00;
	tmp_devp->diff =0;
	file->private_data = tmp_devp;
	tmp_devp->msg_ready =0;
	tmp_devp->read_queued =0;
//	tmp_devp->read_msg = kmalloc(sizeof(char)*64*SIZE;
	return 0;
}

/*
 * Release driver
 */
int tmp_driver_release(struct inode *inode, struct file *file)
{
	//struct tmp_dev *tmp_devp = file->private_data;
	return 0;
}

/*
 * Write to driver
 */

ssize_t tmp_driver_write(struct file *file, const char *buf, size_t count, loff_t *ppos)
{
	struct tmp_dev *tmp_devp = file->private_data;
	tmp_dev_work *work;
	int ret;
	unsigned char *pointer;
	
	printk("Line 239\n");
	if (tmp_devp->busy == 1) return  -1;
	
	/* Acquire read/write capability - Set busy */	
	tmp_devp->busy = 1;
	gpio_set_value_cansleep(27, 1);
	
	/* Copy data from user space onto kernel space*/
	printk("line %d \n", __LINE__);

	pointer = kmalloc(sizeof(char)*64*count, GFP_KERNEL);
	
	ret = copy_from_user((unsigned char*)pointer,buf,sizeof(char)*64*count);
	
	printk("Initiating work\n");
	/* Initialise the work queue */

	work = (tmp_dev_work *)kmalloc(sizeof(tmp_dev_work),GFP_KERNEL);
	INIT_WORK((struct work_struct *)work , tmp_write_function);
	
	/* Tell the work struct about the device struct so that it can write to the appropriate device*/
	work->devp = tmp_devp;
	work->cnt  = (int)count;
	work->buf = pointer;
	ret = queue_work(tmp_devp->wq,(struct work_struct *)work);
	printk("buffer %p\n", pointer);
	return 0;
}


/*
 * Read from driver
 */

ssize_t tmp_driver_read(struct file *file, char *ubuf, size_t count, loff_t *ppos)
{
	struct tmp_dev *tmp_devp = file->private_data ;
	int ret ;
	int flag;
	unsigned char *pointer;
	tmp_dev_work *work;
	pointer = tmp_devp->read_msg;

	
	printk("busy : %d diff : %d ready message: %d \n", tmp_devp->busy,tmp_devp->diff,tmp_devp->msg_ready);
	
	if (tmp_devp->busy ==1){
		printk("DEVICE BUSY\n");
	/*  Nothing to read */
		if (tmp_devp->msg_ready ==0) return -EBUSY;
		
	/* A message is ready to be read */
		if (tmp_devp->msg_ready ==1) {
		printk("Copying to user \n");
		ret = copy_to_user(ubuf,pointer,sizeof(unsigned char)*64*count);
	//	kfree(pointer);
		tmp_devp->msg_ready =0;
		flag =1;
		}
	}
	
	if (tmp_devp->busy ==0){
		printk("DEVICE NOT BUSY\n");	
		if (tmp_devp->msg_ready ==0) flag =0 ;
	/* Read the data and set the flag */
		if (tmp_devp->msg_ready ==1) {
		ret = copy_to_user(ubuf,pointer,sizeof(unsigned char)*64*count);
		kfree(pointer);
		flag =1;
		tmp_devp->msg_ready = 0;
		}
	}
	
	printk("diff : %d ready message: %d \n", tmp_devp->diff,tmp_devp->msg_ready);
	
	if (tmp_devp->read_queued ==0 && tmp_devp->diff > 0 ){
	
	work = (tmp_dev_work *)kmalloc(sizeof(unsigned char)*64*count,GFP_KERNEL);	
	INIT_WORK((struct work_struct *) work,tmp_read_function);
	work->devp = tmp_devp;
	work->cnt = count;
	work->buf = pointer;
	ret = queue_work(tmp_devp->rq,(struct work_struct *)work);
	printk("Queueued \n");
	}

	switch (flag){
		case 0:
		return -EAGAIN;
		break;
		case 1 : 
		return 0;
		break;
	}
	return 0;
}


/*
   Custom IOCTL Implementation 
   code 1 - device free
   code 0 - device busy
   code -1 : address out of memory
 */

static long tmp_driver_ioctl (struct file *file, unsigned int ioctl_num, unsigned long ioctl_param){

	struct tmp_dev *tmp_devp = file->private_data;	
	int ret;
	unsigned char buf[66]; 
	int i ;
	int  address;
	int cnt;
	int addr;
	cnt = 0;
//	addr = &tmp_devp->write_addr;

	switch (ioctl_num) {
		
		case FLASHGETS : // check if device busy , return tmp_devp->busy	
			if (tmp_devp->busy ==0) return 1; 
			else return 0;
		
		case FLASHSETP : // set the address , write and read address
			ret = get_user(address, (int __user*)ioctl_param);
			if (address > 511 || address > tmp_devp->addr) return -1;
			else if(tmp_devp->busy ==1) return 0;
			else {
				tmp_devp->busy = 1;
				tmp_devp->diff = (tmp_devp->addr-address)/64;
				printk("New difference %d\n",tmp_devp->diff);
				tmp_devp->addr = address;
				tmp_devp->busy = 0;
				return 1;
			}
		
		case FLASHGETP : // get the current write and read address
			if (tmp_devp->busy ==1) return 0;
			put_user(tmp_devp->addr,(int __user*)ioctl_param); 
		//	put_user(tmp_devp->write_addr,(unsigned int *) ioctl_param);
			return 1;

		case FLASHERASE : // check of not busy , make a call to write function to write the whole device with 1s
			if(tmp_devp->busy ==1) return 0;
			else {
				tmp_devp->busy = 1;
				/* Initialise starting addresses */
				addr = tmp_devp->addr;
				
				/* Initialise the array to be written*/
				for (i=0;i<63;i++){ buf[i+2]=0x31;}
				buf[65] = '\0'; 
				while (cnt <512){
				/* Read the current write address */
				buf[1] = (unsigned char)(0x00+ addr%256);
				buf[0] = (unsigned char)(0x00 + addr/256);

				// int i2c_master_send ( const struct i2c_client * client, const char * buf, int count);	
				ret = i2c_master_send(tmp_devp->client, buf, 66) ;  
				cnt = cnt +1;
				addr = (addr+64)%32768;
				printk("ERASE address %d \n", addr);
				msleep(10);
				}
				/* Reset the EEPROM address */ 
				tmp_devp->addr = addr;
				tmp_devp->busy=0;
				printk("Address set to %d", addr);
				return 1;
				}
		}
	return 0;
}


/* File operations structure. Defined in linux/fs.h */
static struct file_operations tmp_fops = {
	.owner		= THIS_MODULE,           /* Owner */
	.open		= tmp_driver_open,        /* Open method */
	.release	= tmp_driver_release,     /* Release method */
	.write		= tmp_driver_write,       /* Write method */
	.read		= tmp_driver_read,        /* Read method */
	.unlocked_ioctl	= tmp_driver_ioctl,
};

/*
 * Driver Initialization
 */
int __init tmp_driver_init(void)
{
	int ret;
	/* Request dynamic allocation of a device major number */
	if (alloc_chrdev_region(&tmp_dev_number, 0, 1, DEVICE_NAME) < 0) {
		printk(KERN_DEBUG "Can't register device\n"); return -1;
	}
//	spinlock_t busy = SPIN_LOCK_UNLOCKED;
	/* Populate sysfs entries */
	tmp_dev_class = class_create(THIS_MODULE, DEVICE_NAME);

	/* Allocate memory for the per-device structure */
	tmp_devp = kmalloc(sizeof(struct tmp_dev), GFP_KERNEL);

	if (!tmp_devp) {
		printk("Bad Kmalloc\n"); return -ENOMEM;
	}

	/* Request I/O region */

	/* Connect the file operations with the cdev */
	cdev_init(&tmp_devp->cdev, &tmp_fops);
	tmp_devp->cdev.owner = THIS_MODULE;

	/* Connect the major/minor number to the cdev */
	ret = cdev_add(&tmp_devp->cdev, (tmp_dev_number), 1);

	if (ret) {
		printk("Bad cdev\n");
		return ret;
	}

	/* Send uevents to udev, so it'll create /dev nodes */
	tmp_dev_device = device_create(tmp_dev_class, NULL, MKDEV(MAJOR(tmp_dev_number), 0), NULL, DEVICE_NAME);	

	ret = gpio_request(I2CMUX, "I2CMUX");
	if(ret)
	{
		printk("GPIO %d is not requested.\n", I2CMUX);
	}

	ret = gpio_direction_output(I2CMUX, 0);
	if(ret)
	{
		printk("GPIO %d is not set as output.\n", I2CMUX);
	}
	gpio_set_value_cansleep(I2CMUX, 0); // Direction output didn't seem to init correctly.	

	// Create Adapter using:
	tmp_devp->adapter = i2c_get_adapter(0); // /dev/i2c-0
	if(tmp_devp->adapter == NULL)
	{
		printk("Could not acquire i2c adapter.\n");
		return -1;
	}

	// Create Client Structure
	tmp_devp->client = (struct i2c_client*) kmalloc(sizeof(struct i2c_client), GFP_KERNEL);
	tmp_devp->client->addr = DEVICE_ADDR; // Device Address (set by hardware)
	snprintf(tmp_devp->client->name, I2C_NAME_SIZE, "i2c_schmooh");
	tmp_devp->client->adapter = tmp_devp->adapter;
	
	ret = gpio_request(27, "gpio_LED");
	ret = gpio_direction_output(27, 1);
	gpio_set_value_cansleep(27,0);
	ret = gpio_request(28, "gpio_LED");
	ret = gpio_direction_output(28,1);
	gpio_set_value_cansleep(28,0);
	/* Create a work queue */
	tmp_devp->wq = create_workqueue("write_queue");
	tmp_devp->rq = create_workqueue("read_queue");
//	tmp_devp->read_msg = kmalloc(sizeof(char)*64*2, GFP_KERNEL);
	return 0;
}
/* Driver Exit */
void __exit tmp_driver_exit(void)
{
	/* Clean up work queues */
	flush_workqueue(tmp_devp->wq);
	destroy_workqueue(tmp_devp->wq);
	flush_workqueue(tmp_devp->rq);
	destroy_workqueue(tmp_devp->rq);

	// Close and cleanup
	i2c_put_adapter(tmp_devp->adapter);
	kfree(tmp_devp->client);

	/* Release the major number */
	unregister_chrdev_region((tmp_dev_number), 1);

	/* Destroy device */
	device_destroy (tmp_dev_class, MKDEV(MAJOR(tmp_dev_number), 0));
	cdev_del(&tmp_devp->cdev);
	if(tmp_devp->msg_ready ==1)kfree(tmp_devp->read_msg);
	kfree(tmp_devp);

	/* Destroy driver_class */
	class_destroy(tmp_dev_class);

	gpio_free(I2CMUX);
	gpio_free(27);
	gpio_free(28);
}

module_init(tmp_driver_init);
module_exit(tmp_driver_exit);
MODULE_LICENSE("GPL v2");
