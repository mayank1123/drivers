#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "flash_dev.h"


void i2c_flash_busy(int fd){
int ret;
ret = ioctl (fd,FLASHGETS,NULL);
switch (ret){
	case 0:
		printf("Device busy\n");
		break;
	case 1: 
		printf("Device Not Busy\n");
		break;
}
}

void i2c_flash_get_addr(int fd){
int ret;
int addr;
ret =ioctl (fd, FLASHGETP, &addr);
switch(ret){
	case 0:
		printf("Device busy\n");
		break;
	case 1:
		printf("Current address is : %d \n", addr);
		break;
	default: 
		printf("Default get_addr call \n");
		break;
}
}

void i2c_flash_set_addr(int fd){
int ret;
int addr;
addr = 0;
ret=ioctl(fd,FLASHSETP,&addr);
//printf("returned value : %d\n",ret);
switch(ret){
	case 0:
		printf("Device busy\n");
		break;
	case 1:
		printf("Current  address  set to : %d\n",addr);
		break;
	case -1:
		printf("Address out of range\n");
		break;
}
}
// Example of Interfacing with TMP102
int main(int argc, char** argv)
{
	int i2c_fd = 0;
	int ret = 0;
	int i =0;
	int count =2 ;
	unsigned char buf[64*count];
	for (i =0; i<64;i++){
		buf[i] = 0x45;
	}
	buf[64] ='\0';
	for (i=64;i<128;i++){
		buf[i] = 0x51;
	} 
//	buf[127] = '\0';
	unsigned char r[64*count]; // adding null character to get proper mesg in dmesg 
	
	i2c_fd = open("/dev/i2c_flash", O_RDWR);
	
	if(i2c_fd < 0)
	{
		printf("Error: could not open i2c device.\n");
		return -1;
	}
	
	for (i = 0; i<2;i++){
	ret = write(i2c_fd, buf, count);
	sleep(2);
	i2c_flash_get_addr(i2c_fd);
	sleep(2);
	}
//	sleep(1);
	printf("Came out\n");
	
	i2c_flash_set_addr(i2c_fd);
	sleep(2);
	
	ret = read(i2c_fd, r , count);
	if(ret < 0)
	{
		printf("Error: could not read temp.\n");
	}
	printf("Second read \n");
	sleep(2);
	
	ret= read(i2c_fd,r,count);
	if(ret < 0)
	{
		printf("Error: could not read temp.\n");
	}
	
	printf("Raw Data: %s\n",r+64);  // prints only the first array cause of null character 	
//	i2c_flash_busy(i2c_fd);
//	i2c_flash_get_addr(i2c_fd);
	sleep(1);
	i2c_flash_get_addr(i2c_fd);
	sleep(1);
	printf("Erasing the device\n");
	ioctl(i2c_fd,FLASHERASE,NULL);
	sleep(1);
	i2c_flash_get_addr(i2c_fd);
	sleep(1);
	i2c_flash_set_addr(i2c_fd);
	sleep(1);
	read(i2c_fd,r,count);
	printf("Raw Data: %s\n",r+64);
	sleep(1);
	read(i2c_fd,r,count);
	printf("Raw Data: %s\n",r+64);
	sleep(1);
	close (i2c_fd);
	return 0;
}
