#ifndef HEADER_I2C
#define HEADER_I2C

#include <linux/ioctl.h>

#define MAJOR_NUM 101

#define FLASHGETS _IO(MAJOR_NUM,0)

#define FLASHSETP _IOW(MAJOR_NUM,1,int )

#define FLASHGETP _IOR(MAJOR_NUM,2,int )  // 

#define FLASHERASE _IO(MAJOR_NUM,3)

#define DEVICE_FILE_NAME "i2c_flash"

#endif
